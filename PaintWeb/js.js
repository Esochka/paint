window.addEventListener("load", function onWindowLoad() {


    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");
 

    context.lineCap = "round";
    context.lineWidth = 8;
 

    document.getElementById("clear").onclick = function clear() {
        var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  
        window.location.href=image;
      context.clearRect(0, 0, canvas.width, canvas.height);

    };

     document.getElementById("safe").onclick = function safe() {
        var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  
        window.location.href=image;


    };

    document.getElementById("num").onchange = function() {
      context.lineWidth = document.getElementById("num").value;
      console.log(num);
    };

    document.getElementById('color').oninput = function(){
    myColor = this.value;
    }
 

    canvas.onmousemove = function drawIfPressed (e) {

      var x = e.offsetX;
      var y = e.offsetY;
      var dx = e.movementX;
      var dy = e.movementY;

      if (e.buttons > 0) {
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - dx, y - dy);
        context.stroke();
        context.closePath();
        context.strokeStyle = myColor;

      }
    };


 
});